from django.contrib.auth.models import User

from rest_framework import serializers

from cordonbleuAPIapp.models import (Recipe, RecipeStep, Profile, RecipeIngredientMbship)

class MiniUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')
        #read_only_fields = ('id', 'username')

class ProfileSerializer(serializers.ModelSerializer):
    profile_picture = serializers.ImageField(allow_empty_file=True, allow_null=True)
    class Meta:
        model = Profile
        fields = ('id', 'description', 'profile_picture')

class UserPublicSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(allow_null=True)
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'last_login')
        write_only_fields = ('password',)
        read_only_fields = ('last_login', 'date_joined')

class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(allow_null=True)
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'last_login', 'date_joined', 'profile', 'password')
        write_only_fields = ('password',)
        read_only_fields = ('last_login', 'date_joined')

    def create(self, validated_data):
        """
        Create and return a new 'User' instance, given the validated data.
        """
        user = User.objects.create(username = validated_data.get('username'),
                                   email = validated_data.get('email'))
        user.set_password(validated_data['password'])
        profile_dict = validated_data.get('profile')
        if profile_dict:
            profile = Profile.objects.create(
                    description=profile_dict.get('description'),
                    profile_picture=profile_dict.get('profile_picture'),
                    user=user)
            profile.save()
        return user

    def update(self, instance, validated_data):
        """
        Update and return an existing 'Recipe' instance, given the validated data.
        """
        for field in validated_data:
            if field == 'profile':
                profile_data = validated_data.get('profile')
                profile = Profile.objects.filter(user=instance).first()
                if profile is not None:
                    profile.description = profile_data.get('description')
                    profile.profile_picture = profile_data.get('profile_picture')
                    profile.save()
                else:
                    Profile.objects.create(
                        user=instance,
                        description=profile_data.get('description'),
                        profile_picture=profile_data.get('profile_picture'))
            else:
                instance.__setattr__(field, validated_data.get(field))
        instance.save()
        return instance

class RecipeStepSerializer(serializers.ModelSerializer):
    picture = serializers.ImageField(allow_empty_file=True,
                                     allow_null=True)

    class Meta:
        model = RecipeStep
        fields = ('id', 'name', 'description', 'duration', 'picture', 'order')
        read_only = ('id', 'recipe')

#https://stackoverflow.com/questions/17552380/django-rest-framework-serializing-optional-fields
#https://github.com/PhilipGarnero/django-rest-framework-social-oauth2

class RecipeIngredientMbshipSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeIngredientMbship
        fields = ('id', 'quantity',)
        read_only = ('id',)

class RecipeSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(source='user.username')
    categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    recipesteps = RecipeStepSerializer(many=True, allow_null=True)
    picture = serializers.ImageField(allow_empty_file=True, allow_null=True, use_url=True)
    ingredients = RecipeIngredientMbshipSerializer(many=True, allow_null=True)
    def get_user(self):
        return self.context['request'].user

    def create(self, validated_data):
        """
        Create and return a new 'Recipe' instance, given the validated data.
        """
        recipe = Recipe.objects.create(user = self.get_user(),
                                       name = validated_data.get('name'),
                                       picture = validated_data.get('picture'),
                                       description = validated_data.get('description'),
                                       difficulty_level = validated_data.get('difficulty_level'),
                                       duration = validated_data.get('duration'),
                                      )
        for step in validated_data.get('recipesteps'):
            new_step = RecipeStep.objects.create(
                id = step.get('id'),
                name = step.get('name'),
                description = step.get('description'),
                duration = step.get('duration'),
                picture = step.get('picture'),
                order = step.get('order'),
                recipe = recipe
            )
        return recipe

    def update(self, instance, validated_data):
        """
        Update and return an existing 'Recipe' instance, given the validated data.
        """
        for field in validated_data:
            if field == 'categories':
                for cat in instance.categories:
                    if cat not in validated_data.get(field):
                        instance.categories.remove(cat)
                for cat in validated_data.get(field):
                    if cat not in instance.categories:
                        instance.categories.add(cat)
            elif field == 'recipesteps':
                for step in validated_data.get('recipesteps'):
                    new_step = RecipeStep.objects.get_or_create(
                        id = step.get('id'),
                        name = step.get('name'),
                        description = step.get('description'),
                        duration = step.get('duration'),
                        picture = step.get('picture'),
                        order = step.get('order'),
                        recipe = recipe
                    )
            #elif field == 'ingredients':
            #    for ingredient in validated_data.get('ingredients'):
            #        new_ingredient = Ingredients.objects.get_or_create(
            #            id = ingredient.get('id'),
            #            quantity = ingredient.get('id'),
            #            unit_measure = ingredient.get('unit_measure')
            #        )
            else:
                instance.__setattr__(field, validated_data.get(field))

        instance.save()
        return instance

    class Meta:
        model = Recipe
        fields = ('id', 'user', 'name', 'picture', 'description', 'difficulty_level',
                  'duration', 'categories', 'last_update', 'added_at',
                  'recipesteps', 'username', 'ingredients')
