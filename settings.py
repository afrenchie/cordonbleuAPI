MEDIA_STORAGE_PATH = 'photos/'
REST_FRAMEWORK = {
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',),
    'DEFAULT_PAGINATION_CLASS': ('apps.core.pagination.StandardResultsSetPagination',),
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.TokenAuthentication'
    ),
}

#AUTHENTICATION_BACKENDS = (
    #API Auth
   #'rest_framework_social_oauth2.backends.DjangoOAuth2',
   #'django.contrib.auth.backends.ModelBackend',

    # Google OAuth2
    #'social_core.backends.google.GoogleAuth2',

    # Facebook OAuth2
    #'social_core.backends.facebook.FacebookAppOAuth2',
    #'social_core.backends.facebook.FacebookOAuth2',
#)

#TEMPLATES = [
#    {
#        'OPTIONS': {
#            'context_processors': [
#                'social_django.context_processors.backends',
#                'social_django.context_processors.login_redirect',
#            ],
#        },
#    }
#]
