from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.authtoken import views as authtoken_views
from . import views
from django.conf.urls import include

urlpatterns = [
    #path('', views.index, name='index'),
    #path('get_all_recipes', views.get_all_recipes, name='get_all_recipes'),
    #path('get_recipe_by_id/<int:id>/', views.get_recipe_by_id, name='get_recipe_by_id'),
    path('recipes/', views.RecipeListView.as_view()),
    path('recipes/<int:pk>', views.RecipeDetailView.as_view()),
    path('users/', views.UserListView.as_view()),
    path('users/<int:pk>', views.UserDetailView.as_view()),
    path('users/<str:username>', views.UserDetailView.as_view()),
    path('api-auth/', include('rest_framework.urls')), #api authentification
    path('api-token-auth/', authtoken_views.obtain_auth_token)
    #path('network-auth/', include('rest_framework_social_oauth2.urls')), #social network authentitification
]

urlpatterns = format_suffix_patterns(urlpatterns)
