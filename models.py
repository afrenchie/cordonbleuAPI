import datetime

from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.core.validators import MinValueValidator
from django.utils.safestring import mark_safe

from cordonbleuAPIapp import settings

#TokenAuthentication
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from django.conf import settings

# This code is triggered whenever a new user has been created and saved to the database

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


def picture_path(instance, filename):
    today = datetime.datetime.today()
    #if instance.user:
    #    user = instance.user.id
    return '{0}/{1}-{2}-{3}/{4}'.format(instance.__class__.__name__,
                                                today.year,
                                                today.day,
                                                today.month,
                                                filename
                                                )

class Profile(models.Model):
    user = models.OneToOneField(User,
                                related_name='profile',
                                blank=False,
                                on_delete=models.CASCADE)
    description = models.TextField("Description",
                                   blank=True,
                                   help_text="Quelques mots sur le cuisinier")
    profile_picture = models.ImageField("Image de profil",
                                        blank=True,
                                        upload_to = picture_path,
                                        help_text="Image de profil")
    LEVEL = (
        (1, "débutant"),
        (2, "apprenti cuisinier"),
        (3, "amateur"),
        (4, "chef cuisinier"),
        (5, "chef étoilé"),
    )
    level = models.IntegerField("Niveau de cuisine",
                                           default=3,
                                           choices=LEVEL,
                                           help_text="Niveau de cuisine")
    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % (self.profile_picture.image_url.url))
    image_tag.short_description = 'Appercu'

    def image_thumb(self):
        return mark_safe('<img src="{}" width="80"/>'.format(self.profile_picture.url))
        #return '<img src="/media/%s" width="100" height="100" />' % (self.picture.image_url)
    image_thumb.short_description = 'Appercu'
    def __repr__ (self):
        return 'Profile %s' % self.user.username

    def __str__ (self):
        return self.user.username

    class Meta:
        verbose_name = "Profil cuisinier"
        verbose_name_plural = "Profils cuisiniers"

class RecipeCategory(models.Model):
    name = models.CharField("Nom",
                            max_length=64,
                            blank=False,
                            help_text="Nom de la catégorie")
    def __repr__ (self):
        return 'RecipeCategory %s' % self.name

    def __str__ (self):
        return self.name

    class Meta:
        verbose_name = "Catégorie de recettes"
        verbose_name_plural = "Catégories de recettes"

class IngredientCategory(models.Model):
    name = models.CharField("Nom",
                            max_length=64,
                            blank=False,
                            help_text="Nom de la catégorie")
    def __repr__ (self):
        return 'IngredientCategory %s' % self.name

    def __str__ (self):
        return self.name

    class Meta:
        verbose_name = "Catégorie d'ingrédients"
        verbose_name_plural = "Catégories d'ingrédients"

class Ingredient(models.Model):
    name = models.CharField("Nom",
                            max_length=64,
                            blank=False,
                            help_text="Nom de l'ingrédient")
    categories = models.ManyToManyField(IngredientCategory,
                                        blank=True,
                                        related_name='ingredients',
                                        help_text="Catégories associées")
    picture = models.ImageField("Illustration",
                                        blank=True,
                                        upload_to = picture_path,
                                        help_text="Illustration de la recette")
    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % (self.picture.image_url.url))
    image_tag.short_description = 'Appercu'

    def image_thumb(self):
        return mark_safe('<img src="{}" width="80"/>'.format(self.picture.url))
    image_thumb.short_description = 'Appercu'

    def __repr__ (self):
        return 'Ingredient %s' % self.name

    def __str__ (self):
        return self.name

    class Meta:
        verbose_name = "Ingrédient"
        verbose_name_plural = "Ingrédients"

class Recipe(models.Model):
    user = models.ForeignKey(User,
                             verbose_name="Créateur",
                             on_delete=models.CASCADE,
                             null=True,
                             blank=False,
                             help_text="Créateur de la recette")
    name = models.CharField("Nom",
                            max_length=64,
                            blank=False,
                            help_text="Nom de la recette")
    picture = models.ImageField("Image",
                                null=True,
                                blank=True,
                                upload_to=picture_path,
                                help_text="Image pour illustrer l'étape")
    description = models.TextField("Description",
                                   blank=False,
                                   help_text="Quelques mots pour décrire la recette")
    DIFFICULTY_LEVEL = (
        (1, "débutant"),
        (2, "facile"),
        (3, "moyen"),
        (4, "difficile"),
        (5, "expert"),
    )
    difficulty_level = models.IntegerField("Niveau de difficultée",
                                           default=3,
                                           choices=DIFFICULTY_LEVEL,
                                           help_text="Niveau de difficultée")
    duration = models.DurationField("Durée",
                                    null=True,
                                    blank=True,
                                    help_text="Temps de préparation")
    categories = models.ManyToManyField(RecipeCategory,
                                        verbose_name="Catégories",
                                        related_name='recettes',
                                        blank=True,
                                        help_text="Catégories associées")
    last_update = models.DateTimeField("Dernière mise à jour",
                                       auto_now=True)
    added_at = models.DateTimeField("Date d'ajout",
                                    auto_now_add=True,
                                    editable=False)
    def image_tag(self):
        return mark_safe('<img src="%s" width="150" height="150" />' % (self.picture.image_url.url))
    image_tag.short_description = 'Appercu'

    def image_thumb(self):
        return mark_safe('<img src="{}" width="80"/>'.format(self.picture.url))
        #return '<img src="/media/%s" width="100" height="100" />' % (self.picture.image_url)
    image_thumb.short_description = 'Appercu'

    def __repr__ (self):
        return 'Recipe %s' % (self.name)

    def __str__ (self):
        return '%s' % (self.name)

    class Meta:
        verbose_name = "Recette"
        verbose_name_plural = "Recettes"

class UserRecipeMbship(models.Model):
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             blank=False,
                             null=True)
    recipe = models.ForeignKey(Recipe,
                                on_delete=models.CASCADE,
                                blank=False,
                                null=True)
    favorite = models.BooleanField("Favoris",
                                   default=False)
    MARK = (
        (0, ""),
        (1, "mauvais"),
        (2, "pas terrible"),
        (3, "correct"),
        (4, "bon"),
        (5, "délicieux"),
    )
    mark = models.IntegerField("Note",
                               default=0,
                               choices=MARK)
    def __repr__ (self):
        return 'UserRecipeMbship %s - %s' % (self.user, self.recipe)

    def __str__ (self):
        return '%s - %s' % (self.user, self.recipe)

    class Meta:
        unique_together = (("user", "recipe"),)
        verbose_name = "Relation recette/profil"
        verbose_name_plural = "Relations reccette/profil"

class RecipeStep(models.Model):
    name = models.CharField("Nom",
                            max_length=64,
                            blank=True,
                            help_text="Nom de l'étape")
    picture = models.ImageField("Image",
                                blank=True,
                                help_text="Image pour illustrer l'étape")
    duration = models.DurationField("Durée",
                                    blank=True,
                                    null=True,
                                    help_text="Durée de l'étape")
    description = models.TextField("Description",
                                   blank=False,
                                   help_text="Quelques mots pour décrire l'étape")
    order = models.PositiveIntegerField("Ordre",
                                        blank=False,
                                        help_text="Ordre de l'étape",
                                        validators=[MinValueValidator(1)])
    recipe = models.ForeignKey(Recipe,
                               related_name='recipesteps',
                               verbose_name="Recette associée",
                               blank=False,
                               on_delete=models.CASCADE,)

    def __repr__ (self):
        return 'RecipeStep %s' % self.name

    def __str__ (self):
        return self.name

    class Meta:
        verbose_name = "Étape de recette"
        verbose_name_plural = "Étapes de recettes"

class RecipeIngredientMbship(models.Model):
    ingredient = models.ForeignKey(Ingredient,
                                   verbose_name="Ingrédient",
                                   on_delete=models.CASCADE,
                                   blank=False,
                                   null=True,
                                   help_text="Ingrédient associé",)
    recipe = models.ForeignKey(Recipe,
                               verbose_name="Recette",
                               on_delete=models.CASCADE,
                               blank=False,
                               null=True,
                               help_text="Recette associée",)
    quantity = models.CharField("Quantité",
                            max_length=64,
                            blank=True,
                            help_text="Quantité de l'ingrédient")
    UNIT_MEASURE =  (
        ("-", "-"),
        ("g", "grammes"),
        ("mg", "miligrammes"),
        ("kg", "kilogrammes"),
        ("dz", "douzaine"),
    )
    unit_measure = models.CharField("Unité de mesure",
                            max_length=64,
                            choices=UNIT_MEASURE,
                            blank=True,
                            default="-",
                            help_text="Unité de mesure de l'ingrédient")

    def __repr__ (self):
        return 'RecipeIngredientMbship %s - %s' % (self.ingredient, self.recipe)

    def __str__ (self):
        return '%s - %s' % (self.ingredient, self.recipe)

    class Meta:
        verbose_name = "Relation recette/ingrédient"
        verbose_name_plural = "Relations recettes/ingrédients"
