from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from cordonbleuAPIapp.models import (Profile, Recipe, Ingredient,
    RecipeStep, RecipeCategory, IngredientCategory, UserRecipeMbship,
    RecipeIngredientMbship)
#https://djangotricks.blogspot.fr/2016/12/django-administration-inlines-for-inlines.html
class RecipeStepInline(admin.StackedInline):
    model = RecipeStep
    extra = 1
    fields = (("name", "duration", "order"),"description", "picture")
class RecipeInline(admin.TabularInline):
    model = Recipe
    extra = 1

class ProfileInline(admin.TabularInline):
    model = Profile
    extra = 0
    fields = ('description', ('profile_picture','image_thumb'), 'level')
    readonly_fields = ('image_thumb',)

class UserRecipeMbshipInline(admin.TabularInline):
    model = UserRecipeMbship
    extra = 1
    fields = ("favorite","mark")
    def get_queryset(self, request):
        qs = super(UserRecipeMbshipInline, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)

class RecipeIngredientMbshipInline(admin.TabularInline):
    model = RecipeIngredientMbship
    extra = 1

class RecipeCategoryInline(admin.TabularInline):
    model = Recipe.categories.through
    extra = 1

class IngredientCategoryInline(admin.TabularInline):
    model = Ingredient.categories.through
    extra = 1

class RecipeCategoryAdmin(admin.ModelAdmin):
    pass

class IngredientCategoryAdmin(admin.ModelAdmin):
    pass

class RecipeAdmin(admin.ModelAdmin):
    def last_update_formated(self, obj):
        return obj.last_update.strftime("%d %b %Y %H:%M:%S")

    list_display = ('name','user','last_update_formated')
    fields = ('user',('name','difficulty_level'),'duration','description', ('picture','image_thumb'))
    readonly_fields = ('user','image_thumb')
    inlines = (RecipeStepInline, RecipeIngredientMbshipInline, UserRecipeMbshipInline)
    def get_queryset(self, request):
        qs = super(RecipeAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(user=request.user)
    def save_model(self, request, obj, form, change):
        if obj.user == None:
            obj.user =request.user
        super().save_model(request, obj, form, change)

class IngredientAdmin(admin.ModelAdmin):
    fields = ('name', ('picture', 'image_thumb'), 'categories')
    readonly_fields = ('image_thumb',)
class MyUserAdmin(UserAdmin):
    inlines = (UserRecipeMbshipInline, RecipeInline, ProfileInline)
    #def get_form(self, request, obj=None, **kwargs):
        #print(self.fieldsets)
        #if not request.user.is_superuser:
            #self.exclude = []
            #print(kwargs)
        #    self.exclude.extend(["is_active", "is_staff", "is_superuser", "password", "user_permissions"])
        #return super(MyUserAdmin, self).get_form(request, obj, **kwargs)

    def get_queryset(self, request):
        qs = super(MyUserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(id=request.user.id)

admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
admin.site.register(Recipe, RecipeAdmin)
admin.site.register(IngredientCategory, IngredientCategoryAdmin)
admin.site.register(RecipeCategory, RecipeCategoryAdmin)
admin.site.register(Ingredient, IngredientAdmin)
