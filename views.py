import json

#from rest_framework.renderers import JSONRenderer
#from rest_framework.parsers import JSONParser
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import (Http404, JsonResponse, HttpResponse)
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework.authentication import (SessionAuthentication,
    BasicAuthentication, TokenAuthentication)
from rest_framework.filters import OrderingFilter
from rest_framework import (permissions, status)
from rest_framework.generics import (ListAPIView, RetrieveAPIView)
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import (MultiPartParser, FormParser)
from rest_framework.response import Response
from rest_framework.views import APIView

from cordonbleuAPIapp.serializers import (RecipeSerializer, UserSerializer)
from cordonbleuAPIapp.models import Recipe
from cordonbleuAPIapp.permissions import (IsOwnerOrReadOnly,
    UserIsOwnerOrReadOnly)

#Basic view
def index(request):
    return HttpResponse("Interface administration de cordon bleu")

#@login_required
def get_all_recipes(request):
    if request.method == 'GET':
        data = Recipe.objects.all().select_related('user')
        json_data = dict()
        json_data["data"] = list()
        for r in data:
            json_data["data"].append(r.as_dict())
        json_data["results_count"] = len(data)
        return JsonResponse(json_data, safe=True)

def get_recipe_by_id(request, id=-1):
    if request.method == 'GET' and id != -1:
        data = Recipe.objects.get(pk=id).as_dict()
        return JsonResponse({"data": data}, safe=True)

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 10

class RecipeListView(ListAPIView):
    """
    List all recipes, or create a new recipe.
    """
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    parser_classes = (MultiPartParser, FormParser)
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    filter_backends = (DjangoFilterBackend, OrderingFilter)
    pagination_class = StandardResultsSetPagination
    ordering_fields = ('user', 'name', 'difficulty_level')
    filter_fields = ('user', 'name', 'difficulty_level')

    #def get(self, request, format=None):
    #    recipes = Recipe.objects.all()
    #    serializer = RecipeSerializer(recipes,many=True,context={'request': request})
    #    return Response(serializer.data)

    def post(self, request, format=None):
        serializer = RecipeSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def perform_create(self, recipe):
        recipe.save(user=self.request.user)
        if self.request.data.get('picture') is not None:
            picture = self.request.data.get('picture')
            owner = self.request.user
            serializer.save(owner=owner,picture=picture)


class RecipeDetailView(RetrieveAPIView):
    """
    Retrieve, update or delete a recipe instance.
    """
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer()

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    parser_classes = (MultiPartParser, FormParser)
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                      IsOwnerOrReadOnly)

    def patch(self, request, pk, format=None):
        recipe = self.get_object(pk)
        serializer = RecipeSerializer(recipe, data=request.data, context={'request': request}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_object(self, pk):
        try:
            return Recipe.objects.get(pk=pk)
        except Recipe.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        recipe = self.get_object(pk)
        serializer = RecipeSerializer(recipe)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        recipe = self.get_object(pk)
        serializer = RecipeSerializer(recipe, data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    """
    def delete(self, request, pk, format=None):
        recipe = self.get_object(pk)
        recipe.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    """
    def perform_create(self, recipe):
        if self.request.data.get('picture') is not None:
            picture = self.request.data.get('picture')
            serializer.save(owner=owner, picture=picture)

class UserListView(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    parser_classes = (MultiPartParser, FormParser)
    #permission_classes = (permissions.IsAuthenticated,)

    filter_backends = (DjangoFilterBackend, OrderingFilter)
    filter_fields = ('username', 'first_name', 'last_name', 'email')
    pagination_class = StandardResultsSetPagination
    ordering_fields = ('username', 'first_name', 'last_name', 'email')

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserDetailView(RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    parser_classes = (MultiPartParser, FormParser)
    #permission_classes = (permissions.IsAuthenticated, UserIsOwnerOrReadOnly,)

    lookup_field = ("username")

    def put(self, request, pk, format=None):
        user = request.user
        serializer = UserSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def patch(self, request, pk, format=None):
        user = request.user
        serializer = UserSerializer(user, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
