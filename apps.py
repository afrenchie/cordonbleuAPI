from django.apps import AppConfig


class CordonbleuapiappConfig(AppConfig):
    name = 'cordonbleuAPIapp'
